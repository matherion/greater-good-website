echo ----------
echo $(date)

### Go to directory with cloned git repo
cd ~/website_pulled_repo

### Set the path so LaTeX can be found
PATH=$PATH:/var/www/vhosts/sysrevving.com/.phpenv/shims:/opt/plesk/phpenv/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/var/www/vhosts/sysrevving.com/.TinyTeX/bin/x86_64-linux

### Delete old 'public' directory if it exists
#rm -rf public

pwd
echo $PATH
echo Calling quarto

### Render the site
#/usr/local/bin/quarto render --to all
/usr/local/bin/quarto render --execute --to html --output-dir public

echo Finished quarto.

echo Starting to delete files in /public_html.

### Delete all contents in public HTML directory
rm -rf ~/public_html/*.*
rm -rf ~/public_html/*
rm -f ~/public_html/.htaccess

echo Deleted files. Copying new website files.

### Copy website
cp -RT public ~/public_html

echo Copied new website files. Copying .htaccess file.

### Copy .htaccess
cp -f .htaccess ~/public_html

echo Done.

echo ----------
